/* global can , $ */
"use strict";

var Aux = {
		log(msg){
			$(".responses").append( `${msg}<br/>` );
		}
	},
	host = window.location+"",
	h = host.replace("http", "ws");

var appSocket = new WebSocket(`${h}report`);
appSocket.onopen = function (event) {
  appSocket.send("Socket is open"); 
};

appSocket.onmessage = function (event) {
  try{
	  let _data = JSON.parse(event.data);
	  $(".responses").append( can.view("/resources/mustaches/data.mustache", {data: _data}) );
  }
  catch(e){
  	
  }
  //Aux.log(event.data);
}

var Benchmark = can.Control.extend({
	".cmd-fire click"(element, event){
		event.preventDefault();
		let _url = "/api/benchmark",
			_data = {
				url: $("#url").val(),
				time: $("#time").val(),
				requests: $("#reqs").val(),
				concurrent: $("#concurrency").val()
			};
		$.ajax({
			method: "post",
			url: _url,
			data: JSON.stringify(_data),
			contentType: "application/json",
			success(data){
				Aux.log("Request sent to the server");
			},
			error(err){
				Aux.log("error: "+ err);
			}
		});
	},
	init(){
		
	}
});

var benchmark = new Benchmark("body", {});