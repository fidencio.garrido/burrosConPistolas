module.exports = function(grunt) {

	grunt.initConfig({
		bower:{
			install: {
				options: {
					targetDir: "./public/resources",
					install: true,
					cleanBowerDir: false
				}
			}
		},
		copy:{
			css: {
				files: [
					{ expand: true, cwd: "bower_components/bootstrap/dist", src: ["**"], dest: "public/resources/bootstrap", filter: 'isFile' }
				]
			}
		},
		less: {
			dev: {
				files: {
					"bower_components/bootstrap//bootstrap.css": "public/resources/bootstrap/bootstrap.less"
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-bower-task');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-less');

	grunt.registerTask('default', ['jshint']);

};