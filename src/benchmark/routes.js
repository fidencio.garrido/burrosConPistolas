"use strict";

var service = require("./service");

var Routes = {
	test(req, res){
		service.test( req.body ).then( ()=>{
			res.status(202).send( "In progress..." );
		});
	},
	init(options){
		service.eventHandler = options.eventHandler;
	}
};

module.exports=Routes;