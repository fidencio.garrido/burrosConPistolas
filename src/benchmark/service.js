"use strict";

const exec = require("child_process").exec;

var Service = {
	defaultOpts: {
		requests: 100,
		concurrent: 4,
		time: 0
	},
	eventHandler: null,
	notify(evName, args){
		if(Service.eventHandler!==null){
			Service.eventHandler.emit(evName, args);
		}
	},
	lineWhere(expr, report, args){
		return expr( report, args );
	},
	starts(report, string){
		let lines = report.split("<br/>"),
			val = "";
		lines.forEach( (line)=>{
			if(line.trim().startsWith(string)){
				val = line.replace(string, "");
			}
		});
		return val.trim();
	},
	lineof(report, string){
			let lines = report.split("<br/>"),
			val = -1,
			ndx = 0;
			lines.forEach( (line)=>{
				if(line.trim().startsWith(string)){
					val = ndx;
				}
				ndx++;
			});
			return val;
	},
	parsePercentages(report){
		let lineof = Service.lineof(report, "Percentage of the requests served within a certain time (ms)"),
			lines = report.split("<br/>");
		let group = {};
		for(let i = lineof+1; i< (lineof+10); i++){
			let line = lines[i].split("%");
			let key = (line.length === 2) ? line[0] : "";
			group[key] = (line.length === 2) ? line[1] : "";
		}
		return group;
	},
	parse(response){
		let r = {
			host: Service.lineWhere(Service.starts, response, "Server Hostname: "),
			path: Service.lineWhere(Service.starts, response, "Document Path: "),
			concurrency: Service.lineWhere(Service.starts, response, "Concurrency Level: "),
			totalTime: Service.lineWhere(Service.starts, response, "Time taken for tests: "),
			totalRequests: Service.lineWhere(Service.starts, response, "Complete requests: "),
			failed: Service.lineWhere(Service.starts, response, "Failed requests: "),
			total: Service.lineWhere(Service.starts, response, "Total transferred: "),
			reqsps: Service.lineWhere(Service.starts, response, "Requests per second: "),
			samples: Service.parsePercentages(response)
		};
		return r;
	},
	/**
	 * Removes attributes if they are not empty 
	 */
	getCleanObject(_object){
		let clean = {};
		_object.check.forEach( (attr)=> {
			if ( _object.object[attr] && _object.object[attr].trim()!=="" ){
				clean[attr] = _object.object[attr];
			}
		});
		return clean;
	},
	test(options){
		return new Promise( (resolve, reject)=>{
			let cleanOpts = this.getCleanObject({ object: options, check: ["concurrent", "requests", "url", "time"] }); 
			let _options = {};
			Object.assign(_options, this.defaultOpts, cleanOpts);
			console.log("Starting test with ", JSON.stringify(_options));
			resolve();
			
			exec(`ab -q -t ${_options.time} -r -c ${_options.concurrent} -n ${_options.requests} ${_options.url}`, (error, stdout, stderr) => {
				if (error) {
					console.error(`exec error: ${error}`);
					return;
				}
				console.log(`stdout: ${stdout}`);
				console.log(`stderr: ${stderr}`);
				let msg = stdout.replace(/(\r\n|\n|\r)/gm,"<br/>"),
					json = Service.parse(msg);
				console.log(json);
				Service.notify("report", json);
			});
		});
	}
};

module.exports=Service;