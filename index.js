"use strict";

var express = require("express"),
	bodyParser = require("body-parser"),
	app = express(),
	wsocket = require("express-ws")(app),
	benchmark = require("./src/benchmark/routes"),
	port = process.env.port || 3001;
	
const EventEmitter = require('events');

class AppEmitter extends EventEmitter {};

const appEmitter = new AppEmitter();

appEmitter.on("report", (args) => {
	connections.forEach( (conn)=> {
		conn.send(JSON.stringify(args));
	});
});

var connections = [];
	
var App = {
	initRoutes(){
		benchmark.init({eventHandler: appEmitter});
		app.post("/api/benchmark", benchmark.test);
		app.ws('/report', function(ws, req) {
			connections.push(ws);
			ws.on('message', function(msg) {
				ws.send("Connected to websocket");
			});
		});
	},
	initExpress(){
		app.use(bodyParser.json());
		app.set("view engine", "pug");
		app.use(express.static(__dirname + "/public"));
	},
	initViews(){
		app.get("/", (req, res)=>{
			res.render("home");
		});
	},
	init(){
		this.initExpress();
		this.initRoutes();
		this.initViews();
		app.listen(port, ()=>{
			console.log(`Application started in port ${port}`);	
		});
	}
};

App.init();